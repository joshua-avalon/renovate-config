# Renovate Config

[![License][license_badge]][license]

Shareable Renovate config.

[license]: https://gitlab.com/joshua-avalon/renovate-config/blob/master/LICENSE
[license_badge]: https://img.shields.io/badge/license-Apache--2.0-green.svg
